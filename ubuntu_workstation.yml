---
- name: Ubuntu Workstation
  hosts: localhost
  connection: local

  tasks:
  - name: Initial Setup
    become: true
    block:
    - name: Update all packages to their latest version
      apt:
        name: "*"
        state: latest
        update_cache: yes
    - name: Installing Packages
      apt:
        install_recommends: yes
        pkg: 
        - vim
        - htop
        - curl
        - wget
        - gdebi
        - vlc
        - nmap
        - fonts-powerline
        - apt-transport-https
        - fonts-hack
        - tree
        - tmux
        - apt-transport-https
        - ca-certificates
        - gnupg
        - python3-pip
        - make
        - keepassxc
        - remmina
        - unzip
        - nfs-client
        - cifs-utils
        - git
        - bash-completion
        - gnupg-agent
        - zsh
        - flameshot
        - tilix
        - lsb-release
        - virtualbox

  - name: ZSH
    block:
    - name: Changing Default Shell
      become: true
      user: 
        name: '{{ ansible_user_id }}'
        shell: /bin/zsh
    - name: Downloading Oh My ZSH!
      shell: 
        cmd: 'curl -fsL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash'
        warn: false
    - name: Enable ZSH plugins
      lineinfile:
        path: /home/{{ ansible_user_id }}/.zshrc
        regexp: '^plugins='
        line: 'plugins=(git ansible aws docker docker-compose helm kube-ps1 kubectl kubectx nmap sudo terraform vagrant web-search )'
    - name: Enable Agnoster Theme
      lineinfile:
        path: /home/{{ ansible_user_id }}/.zshrc
        regexp: '^ZSH_THEME='
        line: 'ZSH_THEME="agnoster"'
    - name: Creating ZSH Completion folder
      file:
        path: /home/{{ ansible_user_id }}/.oh-my-zsh/completions
        state: directory
        mode: 0755
    - name: Adding autoload to compinit
      lineinfile:
        path: /home/{{ ansible_user_id }}/.zshrc
        line: 'autoload -U compinit && compinit'
        state: present

  - name: Hashicorp
    become: true
    block:
    - name: Add Hashicorp Key
      become: true
      apt_key:
        url: 'https://apt.releases.hashicorp.com/gpg'
        state: present
    - name: Add Hashicorp Repo
      become: true
      apt_repository:
        repo: 'deb [arch=amd64] https://apt.releases.hashicorp.com {{ ansible_distribution_release }} main'
        state: present
        filename: hashicorp-repo
    - name: Install Hashicorp Tools
      become: true
      apt:
        pkg:
        - terraform
        - vagrant
        update_cache: yes
        state: latest

  - name: Docker
    become: true
    block: 
    - name: Add Docker Key
      apt_key:
        url: 'https://download.docker.com/linux/ubuntu/gpg'
        state: present
    - name: Add Docker Repository
      apt_repository:
        repo: 'deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable'
        state: present
        filename: docker-repo
    - name: Docker Packages
      apt:
        pkg:
        - docker-ce
        - docker-ce-cli
        - containerd.io
        update_cache: yes
        state: latest
    - name: Adding existing user to group Docker
      user: 
        name: '{{ ansible_user_id }}'
        groups: docker 
        append: yes
      ignore_errors: true

  - name: Kubernetes
    become: true
    block: 
      - name: Add Google Key
        apt_key:
          url: 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
          state: present
      - name: Add Kubernetes Repo
        apt_repository:
          repo: 'deb https://apt.kubernetes.io/ kubernetes-xenial main'
          state: present
          filename: kubernetes-repo
      - name: Install Kubectl 
        apt:
          name: kubectl
          update_cache: yes
          state: latest      
      - name: Downloading Kubectx and Kubens 
        git:
          repo: 'https://github.com/ahmetb/kubectx'
          dest: /opt/kubectx 
      - name: Installing kubectx and kubens
        file:
          src: '/opt/kubectx/{{ item }}'
          dest: '/usr/local/bin/{{ item }}'
          state: link
        with_items:
          - kubectx
          - kubens  
      - name: Creating autocompletion for kubens and kubectx
        file:
          src: '/opt/kubectx/completion/_{{ item }}'
          dest: '/home/{{ ansible_user_id }}/.oh-my-zsh/completions/_{{ item }}'
          state: link
        with_items:
          - kubectx.zsh
          - kubens.zsh 

  - name: VSCode
    become: true
    block: 
      - name: Add Microsoft Key
        apt_key:
          url: 'https://packages.microsoft.com/keys/microsoft.asc'
          state: present
      - name: Add VSCode Repo
        apt_repository:
          repo: 'deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main'
          state: present
          filename: vscode-repo
      - name: Install VSCode
        apt:
          name: code
          update_cache: yes
          state: latest

  - name: Google Chrome
    become: true
    block:
      - name: Add Google Key
        apt_key: 
          url: 'https://dl.google.com/linux/linux_signing_key.pub'
          state: present
      - name: Add Chrome Repo
        apt_repository:
          repo: 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main'
          state: present
          filename: chrome-repo
      - name: Install Google Chrome
        apt:
          name: google-chrome-stable
          update_cache: yes
          state: latest
