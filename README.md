## Description

Ansible playbook to set up my personal workstation. The playbook was written targeting an Ubuntu OS installation.

## Requeriments

Ansible client needs to be installed on the workstation.

## Usage

ansible-playbook ubuntu_workstation -K
